import React, {useState} from 'react';

const TestComponent = () => {
    const [inputPrice, setInputPrice] = useState('')
    const [currentCarName, setCarName] = useState('')
    const [error, setError] = useState('')
    const [currentValue, setCurrentValue] = useState(0)

    const cars = [
        {
            imgUrl: 'k3.jpg',
            carName: "k-3",
            price: 25000000
        },
        {
            imgUrl: 'monig.jpg',
            carName: "모닝",
            price: 18000000
        },
        {
            imgUrl: 'ray.jpg',
            carName: "레이",
            price: 18650000
        },
        {
            imgUrl: 'cesiper.jpg',
            carName: "캐스퍼",
            price: 19600000
        },
        {
            imgUrl: 'aban.jpg',
            carName: "아반떼",
            price: 28000000
        },
        {
            imgUrl: 'seltos.jpg',
            carName: "셀토스",
            price: 29000000
        },
        {
            imgUrl: 'k5.jpg',
            carName: "k-5",
            price: 38000000
        },
        {
            imgUrl: 'reg.jpg',
            carName: "렉스턴",
            price: 60000000
        },
        {
            imgUrl: 'g70.jpg',
            carName: "G70",
            price: 51000000
        }
    ]

    const handleInputChange = e => {
        setInputPrice(e.target.value)
        setCarName('')
        setError('')
        setCurrentValue(0)
    }

    const handleCurrentChange = e => {
        setCurrentValue(prevState => prevState + 1)
    }

    const calculateCar = () => {
        const currentPrice = inputPrice - 3000000 - 300000 - 400000
        const cheap = cars.sort((a, b) => a.price - b.price)
        if (currentValue < cheap.length) {
            if (currentPrice >= cheap[currentValue].price) {
                const filterCar = cars.filter(el => el.price / 1.07 <= currentPrice).sort((a, b) => b.price - a.price)
                if (currentPrice >= filterCar[currentValue].price) {
                    setCarName(filterCar[currentValue].carName)
                    setCurrentValue(prevState => prevState + 1)
                }
            } else {
                setError("예산에 맞는 차가 없습니다. 다시 예산을 입력해주세요")
            }
        } else {
            setError("더이상 추천할 차가 없습니다 그냥 걸어 다니세요.")
        }
    }

    return (
        <div>
            <h2>예산 금액에 맞춰 차 추천</h2>
            <input type="number" value={inputPrice} onChange={handleInputChange}/>
            <button onClick={calculateCar}>추천받기</button>

            {error ? <p>{error}</p> : <p>추천차량 : {currentCarName}</p>}
            {currentValue ? <button onClick={() => {handleCurrentChange(); calculateCar();}}>좀금더 싼차 추천</button> : ''}
        </div>

    )
}

export default TestComponent;